KiCad Footprint Scaler
======================

I wrote a small python script that will let you scale new KiCad footprints.  A
scale that is a non-negative floating point number is the first argument.
The second can be the `.kicad_mod` file, or you can input the text from standard
input.  The output is printed to standard output.  Usage is like follows:

    $ ./scale_footprint.py 0.1 logo.kicad_mod > logo_scaled.kicad_mod

or

    $ ./scale_footprint.py 0.1 < logo.kicad_mod > logo_scaled.kicad_mod

This will scale the file `logo.kicad_mod` to 10% of its size and then save it to
the file `logo_scaled.kicad_mod`.

