#!/usr/bin/env python3

import sys, os
import fileinput

keyword = ''
tokens = []
scale = float(sys.argv.pop(1))

for line in fileinput.input():
    for c in line:
        if (c == ' ') or (c == '\n') or (c == '\r'):
            if keyword != '':
                tokens.append(keyword)
                keyword = ''
        elif (c == '(') or (c == ')'):
            if keyword != '':
                tokens.append(keyword)
                keyword = ''
            tokens.append(str(c))
        else:
            keyword += c


def process(lst):
    # recurse base case
    if (len(lst) == 0):
        return ''

    # get the token
    token = lst.pop(0)
    if token == '(':
        return '(' + process(lst)
    elif token == ')':
        return ') ' + process(lst)
    elif token == 'xy':
        # next two are floating point strings
        x = float(lst.pop(0))
        y = float(lst.pop(0))

        x *= scale
        y *= scale

        res = 'xy %f %f'%(x, y)
        return res + process(lst)
    elif token == 'width':
        # next one is a floating point
        w = float(lst.pop(0))

        w *= scale

        res = 'width %f'%w
        return res + process(lst)
    else:
        return ' ' + token + ' ' + process(lst)


sys.setrecursionlimit(2 * len(tokens))
print(process(tokens))
        


